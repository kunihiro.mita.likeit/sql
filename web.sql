CREATE DATABASE usermanagement　DEFAULT CHARACTER SET utf8;

CREATE TABLE user(
id SERIAL Not Null UNIQUE primary key,
login_id varchar(255) Not Null UNIQUE ,
name varchar(255) Not Null,
birth_date DATE Not Null,
password varchar(255) Not Null,
create_date DATETIME Not Null,
update_date DATETIME Not Null
);

INSERT INTO user(id, login_id, name, birth_date, password, create_date, update_date)
VALUES(1, 'admin', '管理者', '2020/10/05', 'satoru1005', now(), now()
);
